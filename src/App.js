import React, { Component } from 'react';
import Puzzle from './Puzzle'
import Layout from './AppBar';

class App extends Component {
  render() {
    return (
      <div>
        <Layout>
          <Puzzle />
        </Layout>
      </div>
    );
  }
}

export default App;
