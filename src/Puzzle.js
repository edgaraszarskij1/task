import React, { Component } from 'react';
import { shuffle } from './shuffle';
import PuzzleCard from './PuzzleCard';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const Card = {
  isHidden: 'hidingCard',
  isShown: 'showingCard',
  isMatch: 'matchingCard',
}

class Puzzle extends Component {
  constructor(props) {
    super(props);
    let newArray = []
    let colors = [
      '#E94C6F', '#1FDA9A', ' #28ABE3', ' #FA6900', '#FDF200',
      '#2B2301', '#75EB00', ' #982395', '#F2EEB3', '#17A697'
    ];

    colors.forEach(function (element, index) {
      newArray.push({ id: index, card: Card.isHidden, backgroundColor: element });
      newArray.push({ id: index + colors.length, card: Card.isHidden, backgroundColor: element });
    });

    newArray = shuffle(newArray);
    this.state = { newArray, stopClick: false, turns: 20 };

  }

  onNewGame = () => {
    let newArray = this.state.newArray.map(puzzleCard => ({
      ...puzzleCard,
      card: Card.isHidden
    }));
    newArray = shuffle(newArray)
    this.setState({ newArray, turns: 20 })
  }

  handleClick = (id) => {
    if (this.state.turns > 0) {
      const cards = (newArray, toChange, newCard) => {
        return newArray.map(puzzleCard => {
          if (toChange.includes(puzzleCard.id)) {
            return {
              ...puzzleCard,
              card: newCard
            };
          }
          return puzzleCard
        });
      }
      this.handleChange(id, cards)
    }
  }

  handleChange = (id, cards) => {
    const foundPuzzleCard = this.state.newArray.find(puzzleCard => puzzleCard.id === id)

    if (this.state.stopClick || foundPuzzleCard.card !== Card.isHidden) {
      return;
    }

    let stopClick = false;

    let newArray = cards(this.state.newArray, [id], Card.isShown);

    const showingCards = newArray.filter((puzzleCard) => puzzleCard.card === Card.isShown);

    const cardID = showingCards.map(puzzleCard => puzzleCard.id)

    if (showingCards.length === 2 && showingCards[0].backgroundColor === showingCards[1].backgroundColor) {
      newArray = cards(newArray, cardID, Card.isMatch)
      this.setState({ turns: this.state.turns - 1 })
    } else if (showingCards.length === 2) {
      let closedCards = cards(newArray, cardID, Card.isHidden);

      stopClick = true;

      this.setState({ newArray, stopClick }, () => {
        setTimeout(() => {
          this.setState({ newArray: closedCards, stopClick: false, turns: this.state.turns - 1 })
        }, 1000);
      });
      return;
    }
    this.setState({ newArray, stopClick })
  }

  render() {

    const newArray = this.state.newArray.map((puzzleCard) => (
      <PuzzleCard
        key={puzzleCard.id}
        showing={puzzleCard.card !== Card.isHidden}
        backgroundColor={puzzleCard.backgroundColor}
        onClick={() => this.handleClick(puzzleCard.id)} />
    ));

    return (
      <div>
        {newArray}
        {this.state.turns > 0 ? <Typography align='center' variant="h3">Turns left: {this.state.turns}</Typography>
          : <Typography align='center' variant="h3" color='error'>No more turns left </Typography>}

        <Button variant="contained" color="primary" fullWidth style={{ position: 'relative', bottom: 0 }}
          onClick={() => this.onNewGame()}>New game</Button>
      </div>
    )
  }
}

export default Puzzle;
