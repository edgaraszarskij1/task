import React from 'react';
import PropTypes from 'prop-types';
import './PuzzleCards.css'

const PuzzleCards = (props) => {
  let style = {};
  if (props.showing) {
    style.backgroundColor = props.backgroundColor;
  }
  return (
    <div className="card-body"
      style={style} onClick={props.onClick}>
    </div>
  );
};

PuzzleCards.propTypes = {
  showing: PropTypes.bool.isRequired,
  backgroundColor: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default PuzzleCards;